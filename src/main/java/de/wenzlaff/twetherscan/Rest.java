package de.wenzlaff.twetherscan;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Der Rest Service für Eterscan.io.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Rest {

	private final static String ETHERSCANN_BALANCE_URL = "https://api.etherscan.io/api?module=account&action=balance&address=";

	private final static String ETHERSCANN_GAS_PRICE_URL = "https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=";

	private final static String ETHERSCANN_ETH_PRICE_URL = "https://api.etherscan.io/api?module=stats&action=ethprice&apikey=";

	private final static String ETHERSCANN_ANZAHL_TRANSAKTIONEN = "https://api.etherscan.io/api?module=proxy&action=eth_getTransactionCount&address=";

	/**
	 * Gibt die Anzahl der Transaktionen für eine Adresse.
	 * 
	 * @param apiToken
	 * @param ethAdresse
	 * @return
	 */
	public static String getAnzahlTransaktionen(String apiToken, String ethAdresse) {
		return ETHERSCANN_ANZAHL_TRANSAKTIONEN + ethAdresse
				+ "&tag=latest&=apikey=" + apiToken;
	}

	/**
	 * Gibt die Adressinfo.
	 * 
	 * @param apiToken
	 * @param ethAdresse
	 * @return
	 */
	public static String getAdressInfo(String apiToken, String ethAdresse) {
		return ETHERSCANN_BALANCE_URL + ethAdresse + "&tag=latest&apikey="
				+ apiToken;
	}

	/**
	 * Liefert den Gas Preis in Wei.
	 * 
	 * @param apiToken
	 * @return
	 */
	public static String getGasPrice(String apiToken) {
		return ETHERSCANN_GAS_PRICE_URL + apiToken;
	}

	/**
	 * Liefert den Kurs.
	 * 
	 * @param apiToken
	 * @return
	 */
	public static String getEthPrice(String apiToken) {
		return ETHERSCANN_ETH_PRICE_URL + apiToken;
	}

	public static HttpResponse<String> getRequest(String url)
			throws IOException, InterruptedException {
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url))
				.method("GET", HttpRequest.BodyPublishers.noBody()).build();

		HttpResponse<String> response = HttpClient.newHttpClient()
				.send(request, HttpResponse.BodyHandlers.ofString());

		return response;
	}
}
