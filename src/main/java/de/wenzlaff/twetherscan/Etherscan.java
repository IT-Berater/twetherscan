package de.wenzlaff.twetherscan;

import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Beispiel Abfrage von Etherscan REST-Api.
 * 
 * The APIs are free for the community with a maximum rate limit of up to 5
 * calls per sec/IP with a valid API key.
 * 
 * https://etherscan.io/myapikey
 * https://openjdk.java.net/groups/net/httpclient/intro.html
 * http://www.wenzlaff.info
 * 
 * @author Thomas Wenzlaff
 *
 */
@Command(name = "TWEtherscan", mixinStandardHelpOptions = true, version = "TWEtherscan 1.0", description = "Abfrage der EtherScan REST-Api.", showDefaultValues = true, footer = {
		"@|fg(green) Thomas Wenzlaff|@",
		"@|fg(red),bold http://www.wenzlaff.info|@" })
public class Etherscan implements Callable<Integer> {

	/**
	 * Eine echte ETH Adresse von mir, für Trinkgeld ;-)
	 */
	private final static String BUY_ME_A_COFFEE = "0x829F9e57c29ab683E964c76160B7B0BaB2727dD2";

	private static final Logger LOG = LogManager.getLogger(Etherscan.class);

	@Option(names = { "-k",
			"--apikey" }, description = "der Etherscan Api Token", required = true)
	private String etherscanApiToken;

	@Option(names = { "-a",
			"--adresse" }, description = "die Ether (ETH) Adresse", defaultValue = BUY_ME_A_COFFEE)
	private String etherAdresse;

	@Option(names = { "-g",
			"--gas" }, description = "Gibt den Gas Preis aus", defaultValue = "true")
	private static boolean isGas;

	@Option(names = { "-p",
			"--preis" }, description = "Gibt den Kurs aus", defaultValue = "true")
	private static boolean isPreis;

	@Option(names = { "-b",
			"--betrag" }, description = "Gibt Betrag zu einer Adresse aus", defaultValue = "true")
	private static boolean isAdress;

	@Option(names = { "-t",
			"--transaktionen" }, description = "Gibt die Anzahl der Transaktionen zu einer Adresse aus", defaultValue = "true")
	private static boolean isAnzahlTransaktionen;

	public static void main(String[] args) throws Exception {
		LOG.info("Start TWEtherscan Internet Abfrage ... ");
		new CommandLine(new Etherscan()).execute(args);
	}

	@Override
	public Integer call() throws Exception {

		if (isGas)
			Ausgabe.printGasPrice(etherscanApiToken);

		if (isPreis)
			Ausgabe.printETHPreis(etherscanApiToken);

		if (isAdress)
			Ausgabe.printAdressBetrag(etherscanApiToken, etherAdresse);

		if (isAnzahlTransaktionen)
			Ausgabe.printAnzahlTransaktionen(etherscanApiToken, etherAdresse);

		return 0;
	}
}
