package de.wenzlaff.twetherscan;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;

/**
 * Liefert die Client Version.
 * 
 * @author Thomas Wenzlaff
 */
public class Version {

	private static final Logger LOG = LogManager.getLogger(Version.class);

	/**
	 * Liefert die Version des Client z.B.
	 * 
	 * Geth/v1.10.9-omnibus-e03773e6/linux-amd64/go1.17.2
	 * 
	 * @param web3jClient
	 * @return die Version in der Form
	 *         Geth/v1.10.9-omnibus-e03773e6/linux-amd64/go1.17.2
	 * @throws IOException
	 */
	public static String getClientVersion(Web3j web3jClient) throws IOException {
		Web3ClientVersion web3ClientVersion = web3jClient.web3ClientVersion().send();
		String version = web3ClientVersion.getWeb3ClientVersion();
		LOG.info("Client Version: " + version);
		return version;
	}
}
