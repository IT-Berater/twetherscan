package de.wenzlaff.twetherscan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.http.HttpService;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Abfragen eines Kontostands über
 * 
 * https://infura.io/dashboard/ethereum/
 * 
 * mit Web3J.
 * 
 * https://etherscan.io/address/0x829f9e57c29ab683e964c76160b7b0bab2727dd2
 *
 * Parameter z,B.
 * 
 * <pre>
 * 
 
	-n mainnet -p eaa4f4c6b7ce453cba56b73b5d558bdc -a 0x829F9e57c29ab683E964c76160B7B0BaB2727dD2		
	-n rinkeby -p eaa4f4c6b7ce453cba56b73b5d558bdc -a 0x6f55a676d5862623bd79446133043AEf905718B8
 * 
 * </pre>
 * 
 * @author Thomas Wenzlaff
 */
@Command(name = "Kontostand", mixinStandardHelpOptions = true, version = "Kontostand Version 1.0", showDefaultValues = true, description = "Kontostand abfragen", exitCodeList = {
		" 0:Successful program execution." }, footer = { "@|fg(green) Thomas Wenzlaff|@", "@|fg(red),bold http://www.wenzlaff.info|@" })
public class Kontostand implements Callable<Integer> {

	private static final Logger LOG = LogManager.getLogger(Kontostand.class);

	@Option(names = { "-n", "--netzwerk" }, required = true, description = "der Netzwerkname (z.B. mainnet oder rinkeby)", defaultValue = "mainnet")
	private static String netzwerkName;

	@Option(names = { "-p", "--projektid" }, required = true, description = "die Projekt Id von Influra.io")
	private static String projektId;

	@Option(names = { "-a", "--adresse" }, required = true, description = "die Adresse die abgefragt wird")
	private static String ethAdresse;

	@Option(names = { "-c", "--client" }, description = "die Web3j Client Info wird ausgegeben")
	private static boolean isClientVersion;

	public static void main(String[] args) throws Exception {
		new CommandLine(new Kontostand()).execute(args);
	}

	@Override
	public Integer call() throws Exception {

		final String ethUrl = "https://" + netzwerkName + ".infura.io/v3/" + projektId;

		Web3j web3jClient = getWeb3Client(ethUrl);

		if (isClientVersion) {
			Version.getClientVersion(web3jClient);
		}

		EthGetBalance kontoAntwort = web3jClient.ethGetBalance(ethAdresse, DefaultBlockParameterName.LATEST).sendAsync().get(10, TimeUnit.SECONDS);

		BigDecimal skalierterStand = new BigDecimal(kontoAntwort.getBalance()).divide(new BigDecimal(1000000000000000000L), 18, RoundingMode.HALF_UP);

		LOG.info(skalierterStand + " Ether, auf Adresse, " + ethAdresse + ", im Netz, " + netzwerkName);
		return 0;
	}

	public Web3j getWeb3Client(String ethUrl) {
		return Web3j.build(new HttpService(ethUrl));
	}
}