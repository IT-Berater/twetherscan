package de.wenzlaff.twetherscan;

import java.io.IOException;
import java.net.http.HttpResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 * Ausgabe im Logfile und auf der Konsole.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Ausgabe {

	private static final Logger LOG = LogManager.getLogger(Ausgabe.class);

	private static final double FAKTOR = 0.000000000000000001;

	private Ausgabe() {
		// nur static Helper
	}

	public static void printAnzahlTransaktionen(String etherscanApiToken,
			String etherAdresse) throws IOException, InterruptedException {
		HttpResponse<String> response = Rest.getRequest(Rest
				.getAnzahlTransaktionen(etherscanApiToken, etherAdresse));
		LOG.debug(response.body());

		JSONObject erg = new JSONObject(response.body());

		// {"result":"0x0","id":1,"jsonrpc":"2.0"}
		LOG.info(erg.getInt("id") + " Transaktion(en) für die Adresse = "
				+ etherAdresse);
	}

	public static void printAdressBetrag(String etherscanApiToken,
			String etherAdresse) throws IOException, InterruptedException {
		HttpResponse<String> response = Rest.getRequest(Rest
				.getAdressInfo(etherscanApiToken, etherAdresse));
		LOG.debug(response.body()); // {"status":"1","message":"OK","result":"4517710000000000"}
									// = 0.0045ETH

		JSONObject erg = new JSONObject(response.body());
		if (erg.getString("status").equals("1")) {
			LOG.info("Stand: " + erg.getLong("result") * FAKTOR
					+ " Ether auf Adresse: " + etherAdresse);
		} else {
			LOG.error("Error " + erg.getString("result")); // https://info.etherscan.com/api-return-errors/
		}
	}

	public static void printETHPreis(String etherscanApiToken)
			throws IOException, InterruptedException {
		HttpResponse<String> responseEthPrice = Rest
				.getRequest(Rest.getEthPrice(etherscanApiToken));
		LOG.debug("JSON Anwort ETH Preis: " + responseEthPrice.body());
		// {"status":"1","message":"OK","result":{"ethbtc":"0.06307","ethbtc_timestamp":"1627842294","ethusd":"2598.86","ethusd_timestamp":"1627842293"}}

		JSONObject ergEth = new JSONObject(responseEthPrice.body());
		JSONObject ergEthResult = ergEth.getJSONObject("result");
		LOG.info("Kurs für 1 ETH: " + ergEthResult.getString("ethusd")
				+ " Dollar");
	}

	public static void printGasPrice(String etherscanApiToken)
			throws IOException, InterruptedException {
		HttpResponse<String> responseGasPrice = Rest
				.getRequest(Rest.getGasPrice(etherscanApiToken));
		// {"status":"1","message":"OK","result":{"LastBlock":"13024190","SafeGasPrice":"37","ProposeGasPrice":"39","FastGasPrice":"42"}}
		JSONObject ergEth = new JSONObject(responseGasPrice.body());
		JSONObject ergEthResult = ergEth.getJSONObject("result");

		LOG.info("Letzter Block Nr.: " + ergEthResult.getString("LastBlock"));
		LOG.info("Gas Preis: " + ergEthResult.getString("SafeGasPrice")
				+ " Gwei, Propose Gas Preis: "
				+ ergEthResult.getString("ProposeGasPrice")
				+ " Gwei, Fast Gas Preis: "
				+ ergEthResult.getString("FastGasPrice") + " Gwei");
	}
}
