package de.wenzlaff.twetherscan;

import org.junit.jupiter.api.Test;

/**
 * Test der CMD.
 * 
 * @author Thomas Wenzlaff
 */
class EtherscanTest {

	@Test
	void testaufrufCmdHilfe() throws Exception {

		String[] parmeter = { "-h" };
		Etherscan.main(parmeter);
	}

}
