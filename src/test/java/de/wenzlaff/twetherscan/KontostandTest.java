package de.wenzlaff.twetherscan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.web3j.protocol.Web3j;

/**
 * Test der Kontostand CMD.
 * 
 * @author Thomas Wenzlaff
 */
class KontostandTest {

	/**
	 * Meine "echte": Buy my a coffee Ethereum (ETH) Adresse auf dem Mainnet.
	 */
	private static final String MAINNET_ADRESSE = "0x829F9e57c29ab683E964c76160B7B0BaB2727dD2";
	/**
	 * Meine Test Projekt ID. Bei häufiger Benutzung bitte eigene ID auf
	 * 
	 * https://infura.io/dashboard/ethereum/
	 * 
	 * anlegen.
	 */
	private static final String PROJEKT_ID = "eaa4f4c6b7ce453cba56b73b5d558bdc";

	/**
	 * Testadresse auf Rinkeby.
	 */
	private static final String RINKEBY_ADRESSE = "0x6f55a676d5862623bd79446133043AEf905718B8";

	/**
	 * Mainnet URL.
	 */
	private static final String MAINNET_URL = "https://mainnet.infura.io/v3/" + PROJEKT_ID;

	@Test
	void testenKeineParameter() throws Exception {

		String[] parmeter = {};
		Kontostand.main(parmeter);
	}

	@Test
	void testenDerHilfe() throws Exception {

		String[] parmeter = { "-h" };
		Kontostand.main(parmeter);
	}

	@Test
	void testenDerVersion() throws Exception {

		String[] parmeter = { "-V" };
		Kontostand.main(parmeter);
	}

	@Test
	void testenDesKontostandAufDemMainnet() throws Exception {

		String[] parmeter = { "-n", "mainnet", "-p", PROJEKT_ID, "-a", MAINNET_ADRESSE };
		Kontostand.main(parmeter);
	}

	@Test
	void testenDesKontostandAufDemMainnetDefault() throws Exception {

		String[] parmeter = { "-p", PROJEKT_ID, "-a", MAINNET_ADRESSE };
		Kontostand.main(parmeter);
	}

	@Test
	void testenDesKontostandAufDemRinkeby() throws Exception {

		String[] parmeter = { "-n", "rinkeby", "-p", PROJEKT_ID, "-a", RINKEBY_ADRESSE };
		Kontostand.main(parmeter);
	}

	@Test
	void testetDieVersionDesClient() throws Exception {

		Kontostand k = new Kontostand();
		Web3j client = k.getWeb3Client(MAINNET_URL);
		assertEquals("Geth/v1.10.15-omnibus-hotfix-f4decf48/linux-amd64/go1.17.6", Version.getClientVersion(client));
	}

	@Test
	void testenDieClientVersion() throws Exception {

		String[] parmeter = { "-p", PROJEKT_ID, "-a", MAINNET_ADRESSE, "-c" };
		Kontostand.main(parmeter);
	}
}
